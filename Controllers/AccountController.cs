﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PharmaStore.Data.Identity;
using PharmaStore.Dtos;

namespace PharmaStore.Controllers
{
    public class AccountController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public AccountController(SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
        }

        [HttpGet]
        public IActionResult Login(string? returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginDto loginDto, string? returnUrl = null)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await _signInManager.PasswordSignInAsync(loginDto.Username, loginDto.Password, loginDto.RememberMe, true);
                    if (result.Succeeded)
                    {+
                        if (!String.IsNullOrEmpty(returnUrl))
                        {
                            return LocalRedirect(returnUrl);
                        }
                        else
                        {
                            var user = await _userManager.FindByNameAsync(loginDto.Username);
                            var roles = await _userManager.GetRolesAsync(user);
                            if (roles.Contains("Customer"))
                            {
                                return RedirectToAction("index", "Home");
                            } 
                            if (roles.Contains("Admin"))
                            {
                                return LocalRedirect("/admin/home/index");
                            }
                        }
                    }
                    if (result.IsLockedOut)
                    {
                        ModelState.AddModelError("", "User account locked out.");
                    }
                    else if (result.IsNotAllowed)
                    {
                        ModelState.AddModelError("", "User is not allowed.");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Invalid Username & Password.");
                    }
                    ViewData["ReturnUrl"] = returnUrl;
                    return View(loginDto);
                }
                else
                {
                    ViewData["ReturnUrl"] = returnUrl;
                    return View(loginDto);
                }
            }
            catch (Exception ex)
            {
                ViewData["ReturnUrl"] = returnUrl;
                ModelState.AddModelError("", "Error Occured : " + ex.Message);
                return View(loginDto);
            }
        }

        [Authorize]
        [HttpGet]
        public IActionResult AccessDenied(string? returnUrl = null)
        {
            return View();
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Logout(string? returnUrl = null)
        {
            try
            {
                await _signInManager.SignOutAsync();
                if (!String.IsNullOrEmpty(returnUrl))
                {
                    return LocalRedirect(returnUrl);
                }
                else
                {
                    return RedirectToAction("login", "account");
                }
            }
            catch (Exception ex)
            {
                return Content("Error Occured : " + ex.Message);
            }
        }

    }
}
