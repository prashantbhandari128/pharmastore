﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace PharmaStore.Migrations
{
    /// <inheritdoc />
    public partial class UserAddes : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "AspNetUsers",
                type: "longtext",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AspNetUsers",
                type: "longtext",
                nullable: false);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AspNetRoles",
                type: "longtext",
                nullable: false);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Discriminator", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "1978ce7b-a822-4863-8d84-5529a3699c1e", "2", "ApplicationRole", "Customer", "CUSTOMER" },
                    { "a4924b32-a68f-4d60-acd1-cf945cdc3ee9", "1", "ApplicationRole", "Admin", "ADMIN" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "Address", "ConcurrencyStamp", "Discriminator", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { "074163ab-90d3-4e2f-b0b6-44fa03991b6e", 0, "BTM", "87d31393-e365-48fd-8351-a21145b78572", "ApplicationUser", "admin@admin.com", false, true, null, null, "ADMIN", "AQAAAAIAAYagAAAAEBlouomK8JfTTAw7GC+KLEphmDJv+Y/+35HKlgneOUnTaXMhakoF9bvNDNzQBOwkqQ==", "9815939112", false, "b7cdf3ef-daa8-40df-9193-4b48e91c9c2e", false, "Admin" },
                    { "beeb852c-abb6-488a-88a9-efb247c00a58", 0, "KVT", "33c0d2c9-c0aa-40f3-a171-27ee6551280c", "ApplicationUser", "customer@customer.com", false, true, null, null, "CUSTOMER", "AQAAAAIAAYagAAAAEFVKgyuMkHOXqmf062ws6/HZpdQ5KdUJIlUZuA7utZOocxSKNSyM3977Pz2bTIIRbg==", "9815939112", false, "1119bc60-af9b-4c34-b46a-342c89fa3300", false, "Customer" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[,]
                {
                    { "a4924b32-a68f-4d60-acd1-cf945cdc3ee9", "074163ab-90d3-4e2f-b0b6-44fa03991b6e" },
                    { "1978ce7b-a822-4863-8d84-5529a3699c1e", "beeb852c-abb6-488a-88a9-efb247c00a58" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "a4924b32-a68f-4d60-acd1-cf945cdc3ee9", "074163ab-90d3-4e2f-b0b6-44fa03991b6e" });

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "1978ce7b-a822-4863-8d84-5529a3699c1e", "beeb852c-abb6-488a-88a9-efb247c00a58" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1978ce7b-a822-4863-8d84-5529a3699c1e");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a4924b32-a68f-4d60-acd1-cf945cdc3ee9");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "074163ab-90d3-4e2f-b0b6-44fa03991b6e");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "beeb852c-abb6-488a-88a9-efb247c00a58");

            migrationBuilder.DropColumn(
                name: "Address",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AspNetRoles");
        }
    }
}
